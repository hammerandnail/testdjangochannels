import websocket
try:
	import thread
except ImportError:
	import _thread as thread
import time
import json
import numpy as np
import re


def on_message(ws, message):
	print(message)

def on_error(ws, error):
	print(error)

def on_close(ws):
	print("### closed ###")

def on_open(ws):
	def run(*args):
		for i in range(3):
			msg = 'thread {:02d} with message No.{:02d}'.format(args[0], i)
			print(msg)
			time.sleep(0.1)
			# img = bytearray(np.random.randint(255, size=10*10))
			img = list(np.random.randint(255, size=10))
			text = {'message': msg}
			text['data'] = re.sub(' +','',str({'img':img}))
			text = json.dumps(text)
			ws.send(text)
		time.sleep(1)
		ws.close()
		print("thread terminating...")

	for i in range(3):
		thread.start_new_thread(run, (i,))


if __name__ == "__main__":
	websocket.enableTrace(True)
	cookies = {'csrftoken': 'empty',
               'sessionid': 'empty',
               'access': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MjQwNDY2MDQsInVzZXJfaWQiOjEsInRva2VuX3R5cGUiOiJhY2Nlc3MiLCJqdGkiOiJiMjdmZGI2ZjRmOTk0ZDQ5YjQxMTliN2NkZDg0YjBhYiJ9.UMfSDQodOqGhI45xgTXbgOKfWbfYEcbljeYQknoDil0'
               }

	cookieS = ''
	for key,value in cookies.items():
		cookieS += '{}={};'.format(key,value)

	ws = websocket.WebSocketApp("ws://localhost:8002/ws/chat/test/",
								on_message = on_message,
								on_error   = on_error,
								on_close   = on_close,
								cookie     = cookieS,
								)
	ws.on_open = on_open
	ws.run_forever()
